package tech.mastertech.ponto.colaborador.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.ponto.colaborador.client.PontoOauthClient;
import tech.mastertech.ponto.colaborador.dto.PontoOauth;
import tech.mastertech.ponto.colaborador.models.Colaborador;
import tech.mastertech.ponto.colaborador.repositories.ColaboradorRepository;

@Service
public class ColaboradorService {

	@Autowired
	private ColaboradorRepository colaboradorRepository;
	@Autowired
	private PontoOauthClient pontoOauthClient;

	public Colaborador setColaborador(PontoOauth pontoOauth) {
		pontoOauthClient.cadastroLogin(pontoOauth);
		
		Colaborador colaborador = new Colaborador();
		colaborador.setFuncional(pontoOauth.getFuncional());
		colaborador.setNome(pontoOauth.getNome());
		colaborador.setCpf(pontoOauth.getCpf());
		colaborador.setCargo(pontoOauth.getCargo());
		colaborador.setCargaHoraria(pontoOauth.getCargaHoraria());

		colaboradorRepository.save(colaborador);

		return colaborador;
	}

	public Colaborador getColaborador(String funcional) {
		Optional<Colaborador> retorno = colaboradorRepository.findById(funcional);

		if (!retorno.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

		return retorno.get();
	}

	public Colaborador alterarColaborador(String funcional, Colaborador colaboradorAlter) {
		Colaborador colaboradorAntigo = getColaborador(funcional);
		colaboradorAntigo.setCargaHoraria(colaboradorAlter.getCargaHoraria());
		colaboradorAntigo.setNome(colaboradorAlter.getNome());
		colaboradorAntigo.setCargo(colaboradorAlter.getCargo());
		return colaboradorRepository.save(colaboradorAntigo);
	}

	public Iterable<Colaborador> getColaboradores() {
		return colaboradorRepository.findAll();
	}
}
