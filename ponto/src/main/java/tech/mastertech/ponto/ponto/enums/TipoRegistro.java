package tech.mastertech.ponto.ponto.enums;

public enum TipoRegistro {
	
	ENTRADA,
	SAIDA;

}
