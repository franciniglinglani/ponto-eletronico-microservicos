package tech.mastertech.ponto.colaborador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class ColaboradorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ColaboradorApplication.class, args);
	}

}
