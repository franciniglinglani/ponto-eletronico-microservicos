package tech.mastertech.pontooauth.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;

import tech.mastertech.pontooauth.services.PontoOauthService;


@Configuration
@EnableAuthorizationServer
public class AuthConfig extends AuthorizationServerConfigurerAdapter{

  @Autowired
  private BCryptPasswordEncoder encoder;
  
  @Autowired
  private AuthenticationManager manager;
  
  @Autowired
  private PontoOauthService pontoOauthService;
  
  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    clients.inMemory()
     .withClient("Colab")
     .secret(encoder.encode("C0l4b0r4d0r"))
     .authorizedGrantTypes("check_token", "password", "refresh_token")
     .scopes("all")
     .and()
     .withClient("Ponto")
     .secret(encoder.encode("P0nt0"))
     .authorizedGrantTypes("check_token", "password", "refresh_token")
     .scopes("all")
     .and()
     .withClient("gateway")
     .secret(encoder.encode("g4t3w4yZuu1"))
     .authorizedGrantTypes("check_token", "password", "refresh_token")
     .scopes("all");
  }
  
  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    endpoints.authenticationManager(manager).userDetailsService((UserDetailsService) pontoOauthService);
  }
}
