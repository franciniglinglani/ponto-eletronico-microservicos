package tech.mastertech.ponto.colaborador.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import tech.mastertech.ponto.colaborador.enums.Cargo;

@Entity
public class Colaborador {

	@Id
	private String funcional;
	@NotNull
	private String nome;
	@CPF
	@NotNull
	private String cpf;
	@NotNull
	@Enumerated(EnumType.STRING)
	private Cargo cargo;
	private int cargaHoraria;
	
	//private List<Ponto> ponto;


	public String getNome() {
		return nome;
	}
	public String getFuncional() {
		return funcional;
	}
	public void setFuncional(String funcional) {
		this.funcional = funcional;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public int getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
}
