package tech.mastertech.ponto.colaborador.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.ponto.colaborador.dto.PontoOauth;
import tech.mastertech.ponto.colaborador.models.Colaborador;
import tech.mastertech.ponto.colaborador.services.ColaboradorService;

@CrossOrigin
@RestController
public class ColaboradorController {
	
	@Autowired
	private ColaboradorService colaboradorService;
	
	@PostMapping("/cadastra")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Colaborador setColaborador(@Valid @RequestBody PontoOauth pontoOauth) {
		return colaboradorService.setColaborador(pontoOauth);
	}
	
	@PatchMapping("/altera/{funcional}")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public Colaborador alteraColaborador(@PathVariable String funcional, @RequestBody Colaborador colaboradorAlter) {
		return colaboradorService.alterarColaborador(funcional, colaboradorAlter);
	}
	
	@GetMapping("/busca")
	@ResponseStatus(code = HttpStatus.OK)
	public Iterable<Colaborador> getColaboradores(){
	    return colaboradorService.getColaboradores();
	  }

	@GetMapping("/busca/{funcional}")
	@ResponseStatus(code = HttpStatus.OK)
	public Colaborador getColaborador(@PathVariable String funcional){
	    return colaboradorService.getColaborador(funcional);
	  }

}
