package tech.mastertech.ponto.ponto.dto;

import tech.mastertech.ponto.ponto.models.Ponto;

public class RetornoPonto {

	private Iterable<Ponto> ponto;

	private String totalHoras;

	public Iterable<Ponto> getPonto() {
		return ponto;
	}

	public void setPonto(Iterable<Ponto> ponto) {
		this.ponto = ponto;
	}

	public String getTotalHoras() {
		return totalHoras;
	}

	public void setTotalHoras(String totalHoras) {
		this.totalHoras = totalHoras;
	}

}
