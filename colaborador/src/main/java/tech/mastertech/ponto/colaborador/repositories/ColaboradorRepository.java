package tech.mastertech.ponto.colaborador.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.ponto.colaborador.models.Colaborador;

public interface ColaboradorRepository extends CrudRepository<Colaborador, String>{

}
