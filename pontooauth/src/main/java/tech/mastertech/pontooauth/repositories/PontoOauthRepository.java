package tech.mastertech.pontooauth.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.pontooauth.models.PontoOauth;

public interface PontoOauthRepository extends CrudRepository<PontoOauth, Integer>{
	
	Optional<PontoOauth> findByfuncional(String funcional);

}
