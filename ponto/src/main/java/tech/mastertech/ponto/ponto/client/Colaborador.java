package tech.mastertech.ponto.ponto.client;

public class Colaborador {

	private Integer funcional;
	private String nome;
	private String cpf;
	private int cargaHoraria;
	public Integer getFuncional() {
		return funcional;
	}
	public void setFuncional(Integer funcional) {
		this.funcional = funcional;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public int getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
}
