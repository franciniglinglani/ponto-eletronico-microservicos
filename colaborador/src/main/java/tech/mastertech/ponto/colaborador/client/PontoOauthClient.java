package tech.mastertech.ponto.colaborador.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import tech.mastertech.ponto.colaborador.dto.PontoOauth;

@FeignClient(name = "pontooauth")
public interface PontoOauthClient {

	 @PostMapping("/crialogin")
	 public PontoOauth cadastroLogin(PontoOauth pontoOauth);
}

