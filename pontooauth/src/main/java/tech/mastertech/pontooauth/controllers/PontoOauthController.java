package tech.mastertech.pontooauth.controllers;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.pontooauth.models.PontoOauth;
import tech.mastertech.pontooauth.services.PontoOauthService;

@RestController
@RequestMapping()
public class PontoOauthController {

	@Autowired
	PontoOauthService pontoOauthService;

	@PostMapping("/crialogin")
	public PontoOauth cadastroLogin(@RequestBody @Valid PontoOauth pontoOauth) {
		return pontoOauthService.cadastroLogin(pontoOauth);
	}

	@GetMapping("/me")
	public Map<String, String> validar(Principal principal) {
		Map<String, String> map = new HashMap<>();

		map.put("funcional", principal.getName());

		return map;
	}
}
