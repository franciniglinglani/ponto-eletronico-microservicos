package tech.mastertech.pontooauth.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class PontoOauth {

	@Id
	private String funcional;
	@NotNull
	@JsonProperty(access = Access.WRITE_ONLY)
	private String senha;

	public String getFuncional() {
		return funcional;
	}

	public void setFuncional(String funcional) {
		this.funcional = funcional;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String nome) {
		this.senha = nome;
	}

}
